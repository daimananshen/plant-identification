// pages/signUser/signUser.js

const util = require("../../utils/util.js");

// sign_num 签到日期
const app = getApp()
var calendarSignData = [];
var signData;
var score = 0;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    calendarSignData: [],
    signData: []
  },
  //初始化
  //当前时间
  getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = "0" + strDate;
    }
    // let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate +
    //   " " + date.getHours() + seperator2 + date.getMinutes() +
    //   seperator2 + date.getSeconds();
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    return currentdate;
  },
  //签到
  calendarSign: function () {
    // 签阅前判断用户是否授权登录
    if (app.globalData.userInfo === null) {
      wx.getUserProfile({
        desc: '用户信息完善',
        success(res) {
          app.globalData.userInfo = res.userInfo;
        }
      })
    } else {
      this.userSign()
    }
  },
  userSign: function () {
    // 把数据给云数据库
    const db = wx.cloud.database({});
    var openid;
    var util = require("../../utils/util.js");
    let date = new Date();
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    this.getCheckedInRecord(this.data.year, this.data.month, this.data.monthDaySize)
    // 签到的时候需要先查询是否存在该用户，如果用户表不存在用户，把用户数据放入user表，否则不需要
    db.collection('user').add({
      data: {
        _id: openid,
        avatarUrl: app.globalData.userInfo.avatarUrl,
        nickName: app.globalData.userInfo.nickName,
        authorizationDate: util.formatTime(new Date())
      },
      success: function (res) {
        console.log(res,'55555555555')
      },
      fail: function (err) {}
    });

    console.log(app.globalData.userInfo, "444444")

    db.collection('everyday_uers').where({
      _openid: app.globalData.userInfo._openid
    }).get({
      success(res1) {
        console.log(res1, this.getNowFormatDate(), "11111")
        if (res1.data[0].lastTime == this.getNowFormatDate()) {
          wx.showToast({
            title: '今日已签到,请勿重复签到',
            icon: 'none',
          })
        } else {
          db.collection('everyday_uers').doc(res1.data[0]._id).update({
            data: {
              lastTime: this.getNowFormatDate()
            },
            success(res) {
              console.log(res, "2222")
            }
          })
        }
      }
    })

    console.log(strDate, this.getNowFormatDate(), "strDate")

    db.collection('everyday_uers').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        nickName: app.globalData.userInfo.nickName,
        avatarUrl: app.globalData.userInfo.avatarUrl,
        lastTime: this.getNowFormatDate(),
        score: score + 1,
        checkDate: this.getNowFormatDate(),
        sign_num: strDate,
        done: true
      },
      success: function (res) {
        wx.showToast({
          title: '签到成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail: function (res) {}
    })

    this.init()
  },
  //获取已签到日期
  getCheckedInRecord: function (year, month, monthDaySize) {
    const db = wx.cloud.database({});
    db.collection('everyday_uers').get({
      success: function (res) {
        let arr = res.data
        wx.setStorageSync("calendarSignData", arr);
        var mydata = [];
        for (let value of arr) {
          score = value.score
          signData = value.sign_num
          mydata.push(signData)
        }
        this.setData({
          calendarSignData: mydata
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '查询本月签到的数据失败',
          icon: 'none',
        })
      }
    })
  },
  init: function () {
    var mydate = new Date();
    var year = mydate.getFullYear();
    var month = mydate.getMonth() + 1;
    var date = mydate.getDate();
    var day = mydate.getDay();
    var nbsp;
    // 重点(网上的nbsp判断不正确)
    if ((date - day) <= 0) {
      nbsp = day - date + 1;
    } else {
      nbsp = 7 - ((date - day) % 7) + 1;
    }
    var monthDaySize;
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
      monthDaySize = 31;
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      monthDaySize = 30;
    } else if (month == 2) {
      // 计算是否是闰年,如果是二月份则是29天
      if ((year - 2000) % 4 == 0) {
        monthDaySize = 29;
      } else {
        monthDaySize = 28;
      }
    };

    calendarSignData = wx.getStorageSync("calendarSignData")
    let mySignData = [];
    for (let value of calendarSignData) {
      signData = value.sign_num;
      mySignData.push(signData)
    }

    this.getCheckedInRecord(year, month, monthDaySize) //获取已签到日期
    this.setData({
      year: year,
      month: month,
      nbsp: nbsp,
      monthDaySize: monthDaySize,
      date: date,
      calendarSignData: mySignData,
      todaySignData: signData,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function () {
    this.init() //初始化
  }
})